<?php

use Illuminate\Http\Request;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TbDht11Controller;
use App\Http\Controllers\TbGasController;
use App\Http\Controllers\TbMovimientoController;
use App\Http\Controllers\TbTarjetaController;
use App\Http\Controllers\TbUltrasonicoController;
use App\Http\Controllers\TbLedController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('register',[AuthController::class,'register']);
    Route::post('logout',[AuthController::class,'logout']);
    Route::post('refresh',[AuthController::class,'refresh']);
    Route::post('me',[AuthController::class,'me']);
    Route::post('login',[AuthController::class,'login']);

    Route::get('dht11',[TbDht11Controller::class,'index']);
    Route::post('dht11',[TbDht11Controller::class,'store']);
    Route::get('dht11/{id}',[TbDht11Controller::class,'show']);
    Route::put('dht11/{id}',[TbDht11Controller::class,'update']);
    Route::delete('dht11/{id}',[TbDht11Controller::class,'destroy']);
    
    Route::get('Gas',[TbGasController::class,'index']);
    Route::post('Gas',[TbGasController::class,'store']);
    Route::get('Gas/{id}',[TbGasController::class,'show']);
    Route::put('Gas/{id}',[TbGasController::class,'update']);
    Route::delete('Gas/{id}',[TbGasController::class,'destroy']);

    Route::get('Movimiento',[TbMovimientoController::class,'index']);
    Route::post('Movimiento',[TbMovimientoController::class,'store']);
    Route::get('Movimiento/{id}',[TbMovimientoController::class,'show']);
    Route::put('Movimiento/{id}',[TbMovimientoController::class,'update']);
    Route::delete('Movimiento/{id}',[TbMovimientoController::class,'destroy']);

    Route::get('Tarjeta',[TbTarjetaController::class,'index']);
    Route::post('Tarjeta',[TbTarjetaController::class,'store']);
    Route::get('Tarjeta/{id}',[TbTarjetaController::class,'show']);
    Route::put('Tarjeta/{id}',[TbTarjetaController::class,'update']);
    Route::delete('Tarjeta/{id}',[TbTarjetaController::class,'destroy']);

    Route::get('Distancia',[TbUltrasonicoController::class,'index']);
    Route::post('Distancia',[TbUltrasonicoController::class,'store']);
    Route::get('Distancia/{id}',[TbUltrasonicoController::class,'show']);
    Route::put('Distancia/{id}',[TbUltrasonicoController::class,'update']);
    Route::delete('Distancia/{id}',[TbUltrasonicoController::class,'destroy']);

    Route::get('led',[TbLedController::class,'index']);
    Route::post('led',[TbLedController::class,'store']);
    Route::get('led/{id}',[TbLedController::class,'show']);
    Route::put('led/{id}',[TbLedController::class,'update']);
    Route::delete('led/{id}',[TbLedController::class,'destroy']);

});