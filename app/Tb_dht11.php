<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tb_dht11 extends Model
{
    protected $fillable = [
        'humedad', 'temperatura'
    ];
}
