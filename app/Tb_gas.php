<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tb_gas extends Model
{
    protected $fillable = [
        'gas'
    ];
}
