<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tb_movimiento extends Model
{
    protected $fillable = [
        'movimiento'
    ];
}
