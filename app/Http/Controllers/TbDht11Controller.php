<?php

namespace App\Http\Controllers;

use App\Tb_dht11;
use Illuminate\Http\Request;

class TbDht11Controller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Tb_dht11::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tb_dht11::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'dht guardado'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tb_dht11 $Tb_dht11)
    {
        return response()->json([
            'res'=>true,
            'dht'=>$Tb_dht11
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tb_dht11 $Tb_dht11)
    {
        $Tb_dht11->update($Tb_dht11->all());
        return response()->json([
            'res'=>true,
            'msg'=>'dht actualizado'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tb_dht11 $Tb_dht11)
    {
        $Tb_dht11->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'dht eliminado'
        ],200);
    }
}
