<?php

namespace App\Http\Controllers;

use App\Tb_ultrasonico;
use Illuminate\Http\Request;

class TbUltrasonicoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Tb_ultrasonico::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tb_ultrasonico::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'distancia guardado'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tb_ultrasonico $Tb_ultrasonico)
    {
        return response()->json([
            'res'=>true,
            'distancia'=>$Tb_ultrasonico
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tb_ultrasonico $Tb_ultrasonico)
    {
        $Tb_ultrasonico->update($Tb_ultrasonico->all());
        return response()->json([
            'res'=>true,
            'msg'=>'distancia actualizado'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tb_ultrasonico $Tb_ultrasonico)
    {
        $Tb_ultrasonico->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'distancia eliminado'
        ],200);
    }
}
