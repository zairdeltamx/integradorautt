<?php

namespace App\Http\Controllers;

use App\Tb_gas;
use Illuminate\Http\Request;

class TbGasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Tb_gas::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tb_gas::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'gas guardado'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tb_gas $Tb_gas)
    {
        return response()->json([
            'res'=>true,
            'gas'=>$Tb_gas
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tb_gas $Tb_gas)
    {
        $Tb_gas->update($Tb_gas->all());
        return response()->json([
            'res'=>true,
            'msg'=>'gas actualizado'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tb_gas $Tb_gas)
    {
        $Tb_gas->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'gas eliminado'
        ],200);
    }
}
