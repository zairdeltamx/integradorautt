<?php

namespace App\Http\Controllers;

use App\Tb_movimiento;
use Illuminate\Http\Request;

class TbMovimientoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Tb_movimiento::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tb_movimiento::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'movimiento guardado'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tb_movimiento $Tb_movimiento)
    {
        return response()->json([
            'res'=>true,
            'movimiento'=>$Tb_movimiento
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tb_movimiento $Tb_movimiento)
    {
        $Tb_movimiento->update($Tb_movimiento->all());
        return response()->json([
            'res'=>true,
            'msg'=>'movimiento actualizado'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tb_movimiento $Tb_movimiento)
    {
        $Tb_movimiento->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'movimiento eliminado'
        ],200);
    }
}
