<?php

namespace App\Http\Controllers;

use App\Tb_tarjeta;
use Illuminate\Http\Request;

class TbTarjetaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Tb_tarjeta::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tb_tarjeta::create($request->all());
        return response()->json([
            'res'=>true,
            'msg'=>'tarjeta guardado'
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tb_tarjeta $Tb_tarjeta)
    {
        return response()->json([
            'res'=>true,
            'tarjeta'=>$Tb_tarjeta
        ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tb_tarjeta $Tb_tarjeta)
    {
        $Tb_tarjeta->update($Tb_tarjeta->all());
        return response()->json([
            'res'=>true,
            'msg'=>'tarjeta actualizado'
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tb_tarjeta $Tb_tarjeta)
    {
        $Tb_tarjeta->delete();
        return response()->json([
            'res'=>true,
            'msg'=>'tarjeta eliminado'
        ],200);
    }
}
